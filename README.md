# Playing with the ESP32 microcontroller

Everything is coded using micropython

Integrated OLED:
-Display text
-Display an image
-Display an animation

Web server:
-Start a minimalist web server to turn on/off the integrated led
-Reading serial data 

Hall Effect Counter, basic magnetism detector that transfers data from the ESP32 to the pc via python code