
from machine import I2C, Pin
import ssd1306
import esp32
import time

rst = Pin(16, Pin.OUT)
rst.value(1)

esp32.hall_sensor() #Measure Hall effect

i2c = I2C(-1, Pin(15), Pin(4))
display = ssd1306.SSD1306_I2C(128, 64, i2c)
display.fill(0)
display.text('Hall', 21,25)
display.text('Effect', 19,35)
display.show()


#the esp32 is just too slow to execute this function
"""
def displayText(string1,string2):
  
  def __init__():
    string1=''
    string2=''
  
  i2c = I2C(-1, Pin(15), Pin(4))
  display = ssd1306.SSD1306_I2C(128, 64, i2c)
  display.fill(0)
  #Set the appropiate parameters for x,y coordinated according to font size
  display.text(str(string1), 0,5)
  display.text(str(string2), 0,15)
  display.show()
"""  
  
  

#Calibrate the sensor to neglect local magnestism
i=0
local_magnetism=[]
while i<1000:
  i+=1
  time.sleep_ms(0) 
  local_magnetism.append(esp32.hall_sensor())
  
#print(local_magnetism,'\n')
m_avg=sum(local_magnetism) / len(local_magnetism)
print('Local magnetism measure =',m_avg)


#All further measures of magnetism should exclude the mean value ofc
"""
i=0
while i<10:
  i+=1
  time.sleep_ms(1000) 
  m=esp32.hall_sensor()-m_avg
  print(i,m) #it seems the integrated hall sensor has high variance
  
#I will try a different approach, im going to setup a simple low-high-pass filter
"""

i=0
j=0
while i<60:
  i+=1
  time.sleep_ms(500) 
  m=esp32.hall_sensor()
  if m > max(local_magnetism) or m < min(local_magnetism):
    j+=1
    print(i,1) #it seems the integrated hall sensor has high variance
    #display.text('Hall', 21,25)
    for n in range(80,120):
      for m in range(25,45):
        display.pixel(n,m,0)
    display.text(str(j), 90,35)
    display.show()

  else:
    print(i,0)

  

